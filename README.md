# Galápagos Finch RNA-Seq 
## Effects of Urbanization on Host Resistance to Invasive Parasites in the Galápagos Islands
The objective of this analysis was to identify differentially expressed genes in nestling finches in relation to several conditions: urban, nonurban, sham-fumigated, fumigated. Results will support whether urbanization has an effect on _Philornis downsi_, an invasive parasite known to invade nests of finches and draw blood from nestlings. <br>
<br>
    **Path to working directory:** /labs/Knutie/Vaziri/finch_RNA
<br>
## 1. Fastq Lane Merging
The paired-end raw reads were first merged together for both species using the following script:

[fastq_lane_merging.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/01_merging/fastq_lane_merging.sh)
<pre style="color: silver; background: black;">
cd ~/raw_reads

for i in $(find ./ -type f -name "*.fastq.gz" | while read F; do basename $F | rev | cut -c 22- | rev; done | sort | uniq)

    do echo "Merging R1"

cat "$i"_L00*_R1_001.fastq.gz >> ../merged_raw_reads/"$i"_ME_L001_R1_001.fastq.gz

       echo "Merging R2"

cat "$i"_L00*_R2_001.fastq.gz >> ../merged_raw_reads/"$i"_ME_L001_R2_001.fastq.gz

done;
</pre>

### Fastqc and Multiqc
Merged raw reads were then run through fastqc and multiqc for quality control checks

1. Fastqc: [raw_fastqc.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/02_fastqc/01_raw_fastqc.sh)
<pre style="color: silver; background: black;">
## Changing directories to where the fastq files are located
cd ~/merged_raw_reads/

## Loading modules required for script commands
module load fastqc

# output directory
OUTDIR=../../results/merged_fastqc_outputs/raw
mkdir -p $OUTDIR

## Running FASTQC
fastqc -t 6 -o $OUTDIR *.fastq.gz
</pre>

2. Multiqc: [before_multiqc.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/02_fastqc/02_before_multiqc.sh)
<pre style="color: silver; background: black;">
module load MultiQC/1.1
cd ~/results/merged_fastqc_outputs

#output directory
OUTDIR=~/results/merged_multiqc_outputs
mkdir -p $OUTDIR

multiqc --outdir $OUTDIR .
</pre>

## 2. Trimming Raw Reads with Trimmomatic
Trimmomatic was used to used to trim and crop Illumina (FASTQ) data:
[trimmomatic2.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/03_trimmomatic/trimmomatic2.sh)
<pre style="color: silver; background: black;">
cd ~/merged_raw_reads

module load Trimmomatic/0.39

for infile in *_R1_001.fastq.gz
 do
 base=$(basename ${infile} _R1_001.fastq.gz)
 java -jar $Trimmomatic PE -threads 14 -phred33 ${infile} ${base}_R2_001.fastq.gz \
 ${base}_R1_001.paired.fastq.gz ${base}_R1_001.unpaired.fastq.gz \
 ${base}_R2_001.paired.fastq.gz ${base}_R2_001.unpaired.fastq.gz \
 ILLUMINACLIP:/home/FCAM/gvaziri/scripts/TruSeq3-PE.fa:2:30:10 LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:45
done

#output directory
OUTDIR=~/results/trimmomatic_outputs
mkdir -p $OUTDIR

#mv outputs to new directory
mv *paired.fastq.gz $OUTDIR
</pre>

### Fastqc and Multiqc
Fastqc and Multiqc were run against the newly trimmed reads with the following scripts:
1. Fastqc: [trimmed_fastqc.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/02_fastqc/03_trimmed_fastqc.sh)
2. Multiqc: [after_multiqc.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/02_fastqc/04_after_multiqc.sh)

## 3. Aligning Reads to a Genome Using HISAT2
The trimmed reads were next aligned to the reference genome: _Geospiza fortis_ for nestlings. 
[aligner.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/04_hisat2/aligner.sh)
<pre style="color: silver; background: black;">
echo "host name : " `hostname`
echo This is array task number $SLURM_ARRAY_TASK_ID
date


# load software----------------------------------------------
module load hisat2/2.2.1       
module load samtools/1.10

# set input/output files, directories------------------------

# input fastq file directory
INDIR=/home/FCAM/gvaziri/results/trimmomatic_outputs

# output directory
OUTDIR=../../results/hisat2results/alignments
mkdir -p $OUTDIR

# reference genome
GENOME=/isg/shared/databases/alignerIndex/animal/geospiza_fortis/current/HISAT2/geospiza_fortis

# fastq file bash array
FQS=($(find ${INDIR} -name "*L001_R1_001.paired.fastq.gz" | sort))

# fastq files
FQ1=${FQS[$SLURM_ARRAY_TASK_ID]}
echo $FQ1

# get corresponding sample ID
SAM=$(basename $FQ1 | sed 's/L00.*_R.*.paired.fastq.gz//')
echo $SAM

# specify fq files
fq1=$FQ1
fq2=$(echo $fq1 | sed 's/R1_001.paired.fastq.gz/R2_001.paired.fastq.gz/')
#fq1b=$(echo $fq1 | sed 's/L001_R1_001.paired.fastq.gz/L002_R1_001.paired.fastq.gz/')
#fq2b=$(echo $fq1 | sed 's/L001_R1_001.paired.fastq.gz/L002_R2_001.paired.fastq.gz/')

echo $fq1
echo $fq2
#echo $fq1b
#echo $fq2b

#outdir
outdir=$OUTDIR

# reference genome
genome=$GENOME

# output root, directory

outroot=$SAM
echo $outroot


#execute alignment and pipe to samtools
 
 hisat2 -x $GENOME -1 ${fq1} -2 ${fq2} | \
 samtools view -S -h -u - | \
 samtools sort -@ 4 -T $outdir/$outroot - \
 >$outdir/$outroot.bam

# index bam file
 samtools index $outdir/$outroot.bam
</pre>

### Total Reads (Pre and Post-QC) and Alignment Rates
The following table shows total reads before and after quality control, as well as how well each sample aligned with the reference genome:
|Sample|	Total Reads Pre-QC|	Total Reads Post-QC|	Alignment Rate|
|---|	----| ------|-----|		
|SAK0159_S75|	26848858|	25942013|	0.918|
|SAK0060_S23|	18863623|	18026570|	0.86|
|SAK0152_S25|	22278342|	21455609|	0.878|
|SAK0157_S39|	22145819|	21186414|	0.86|
|SAK0167_S48|	23499497|	22670471|	0.869|
|SAK0171_S26|	19022442|	18397526|	0.865|
|SAK0176_S54|	19662231|	18899534|	0.841|
|SAK0181_S22|	20187913|	19354348|	0.858|
|SAK0189_S19|	19131752|	18209251|	0.857|
|SAK0191_S18|	21491783|	20673627|	0.853|
|SAK0194_S33|	21301972|	20394515|	0.85|
|SAK0198_S17|	26222486|	25017039|	0.863|
|SAK0200_S16|	28389046|	26676144|	0.856|
|SAK0207_S8|	21819109|	20836487|	0.847|
|SAK0210_S42|	19505710|	18631390|	0.855|
|SAK0218_S56|	26024025|	25059514|	0.874|
|SAK0222_S3|	22187688|	21247944|	0.854|
|SAK0223_S2|	24165607|	22980306|	0.842|
|SAK0225_S36|	21836556|	20931210|	0.877|
|SAK0245_S29|	19343382|	18606756|	0.873|
|SAK0247_S53|	21698902|	20841716|	0.874|
|SAK0250_S51|	20416550|	19622930|	0.871|
|SAK0304_S58|	21614650|	20791300|	0.86|
|SAK0307_S59|	21184654|	20383881|	0.872|
|SAK0308_S57|	17607026|	16772670|	0.851|
|SAK0311_S11|	26553445|	25405743|	0.84|
|SAK0313_S14|	25277694|	24240886|	0.866|
|SAK0317_S6|	26843299|	25689072|	0.86|
|SAK0326_S47|	21971053|	19799958|	0.866|
|SAK0328_S41|	26248067|	24532889|	0.867|
|SAK0330_S65|	17186825|	16409058|	0.854|
|SAK0331_S52|	19901554|	19132563|	0.868|
|SAK0333_S55|	20280118|	19506047|	0.859|
|SAK0341_S50|	18030947|	17293197|	0.877|
|SAK0347_S70|	18440662|	17537461|	0.807|
|SAK0352_S64|	21398182|	20582596|	0.871|
|SAK0354_S38|	16705859|	15863427|	0.87|
|SAK0358_S60|	21481516|	20690347|	0.86|
|SAK0362_S34|	20234296|	19374380|	0.844|
|SAK0364_S20|	43727416|	42156991|	0.863|
|SAK0367_S9|	23041035|	22026003|	0.86|
|SAK0612_S46|	19764787|	18900722|	0.87|


## 4. Calculating Counts and Number of Reads Mapped with HTSeq
To see how well the reads mapped to the reference genome, HTSeq was run on the alignment files. 
<br>
The following GTF file was used: **GCF_000277835.1_GeoFor_1.0_genomic.gtf** 
- genome-build-accession NCBI_Assembly:GCF_000277835.1
- annotation-source NCBI Geospiza fortis Annotation Release 102

[htseq.sh](https://gitlab.com/PlantGenomicsLab/galapagos-finch-rna-seq/-/blob/main/Scripts/05_htseq/htseq.sh)

<pre style="color: silver; background: black;">
echo "host name : " `hostname`
echo This is array task number $SLURM_ARRAY_TASK_ID
date
# load software----------------------------------------------
module load htseq/0.13.5
#cd into input directory
cd ~/results/hisat2results/alignments
# output directory
OUTDIR=~/results/htseq_counts
mkdir -p $OUTDIR

# bam file bash array
BAM=($(find -name "*_.bam"))
# bam files
BAM1=${BAM[$SLURM_ARRAY_TASK_ID]}
echo $BAM1

# get corresponding sample ID
SAM=$(basename $BAM1 | sed 's/_.bam//')
echo $SAM
#execute htseq command
htseq-count -s no -r pos -f bam --type exon $BAM1 GCF_000277835.1_GeoFor_1.0_genomic.gtf > $OUTDIR/$SAM.counts
</pre>

## 5. Differential Expression Analysis with DESeq2
Nestling finch counts data were utilized to determine the effect of the following:
1. Urban: Fumigated vs. Sham-Fumigated
2. Non-urban: Fumigated vs. Sham-Fumigated
3. Sham-Fumigated: Non-urban vs. Urban
4. Fumigated: Non-urban vs. Urban
<br>
The following script was used in RStudio: [darwin_finch_rnaseq.R](Scripts/07_DESeq2/darwin_finch_rnaseq.R)

<br>
The results of this differential expression analysis can be viewed here: 
<br>
<br>
[`DESeq2 Results`](https://docs.google.com/spreadsheets/d/1Eh0P81iWLbnEDZlDYsm_CTAzgwCKjVaxUqg6lcuHEPY/edit#gid=1802351960)

## 6. GO Enrichment Analysis
[ClueGO](https://apps.cytoscape.org/apps/cluego) was run on sham-fumigated: urban vs non-urban differentially expressed genes to identify significant enriched immune functional categories (GO_ImmuneSystemProcess-EBI-UniProt-GOA-ACAP-ARAP_12.10.2022_00h00).

[`ClueGO Results`](https://docs.google.com/spreadsheets/d/1s2X9yPGRfV0Hz82l7JINkWQsA_EUft8pp4D8lpUl404/edit#gid=1625420216)

An enrichment analysis was also performed on genes from two co-expression modules (blue and turquoise) exhibiting strong correlations to the alive trait. See below.

## 7. Co-expression Analysis with WGCNA
Normalized RNA-seq counts were used as input for a WGCNA co-expression analysis with the following script: [WGCNA R Script](Scripts/08_WGCNA/co_expression.R)

Correlations in transcript levels across 12 traits (urban, sham-fumigated, hemoglobin level, glucose level, alive, deltaC, deltaN, C:N, days old, no. of parasites in nest, bill surface area and scaled mass) introduced 44 modules, which can be seen in the following spreadsheet:

[`WGCNA Results`](https://docs.google.com/spreadsheets/d/1mehqsXDjHIHMwCrCO_qzxzfFFBWtBIBqBhUM2ztRtag/edit#gid=1017548453)








