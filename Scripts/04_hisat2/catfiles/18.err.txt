We recommend using the xeon partition (rather than general or himem) to run this application (hisat2)
/usr/bin/env: python3: No such file or directory
18335419 reads; of these:
  18335419 (100.00%) were paired; of these:
    4065943 (22.18%) aligned concordantly 0 times
    13296880 (72.52%) aligned concordantly exactly 1 time
    972596 (5.30%) aligned concordantly >1 times
    ----
    4065943 pairs aligned concordantly 0 times; of these:
      787136 (19.36%) aligned discordantly 1 time
    ----
    3278807 pairs aligned 0 times concordantly or discordantly; of these:
      6557614 mates make up the pairs; of these:
        4454322 (67.93%) aligned 0 times
        1734019 (26.44%) aligned exactly 1 time
        369273 (5.63%) aligned >1 times
87.85% overall alignment rate
[bam_sort_core] merging from 16 files and 4 in-memory blocks...
