We recommend using the xeon partition (rather than general or himem) to run this application (hisat2)
/usr/bin/env: python3: No such file or directory
21243484 reads; of these:
  21243484 (100.00%) were paired; of these:
    5635743 (26.53%) aligned concordantly 0 times
    14195128 (66.82%) aligned concordantly exactly 1 time
    1412613 (6.65%) aligned concordantly >1 times
    ----
    5635743 pairs aligned concordantly 0 times; of these:
      1006348 (17.86%) aligned discordantly 1 time
    ----
    4629395 pairs aligned 0 times concordantly or discordantly; of these:
      9258790 mates make up the pairs; of these:
        6458860 (69.76%) aligned 0 times
        2272283 (24.54%) aligned exactly 1 time
        527647 (5.70%) aligned >1 times
84.80% overall alignment rate
[bam_sort_core] merging from 16 files and 4 in-memory blocks...
