We recommend using the xeon partition (rather than general or himem) to run this application (hisat2)
/usr/bin/env: python3: No such file or directory
19354348 reads; of these:
  19354348 (100.00%) were paired; of these:
    4669705 (24.13%) aligned concordantly 0 times
    13405955 (69.27%) aligned concordantly exactly 1 time
    1278688 (6.61%) aligned concordantly >1 times
    ----
    4669705 pairs aligned concordantly 0 times; of these:
      715304 (15.32%) aligned discordantly 1 time
    ----
    3954401 pairs aligned 0 times concordantly or discordantly; of these:
      7908802 mates make up the pairs; of these:
        5482349 (69.32%) aligned 0 times
        1915271 (24.22%) aligned exactly 1 time
        511182 (6.46%) aligned >1 times
85.84% overall alignment rate
[bam_sort_core] merging from 16 files and 4 in-memory blocks...
