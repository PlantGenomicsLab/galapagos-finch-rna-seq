We recommend using the xeon partition (rather than general or himem) to run this application (hisat2)
20187900 reads; of these:
  20187900 (100.00%) were paired; of these:
    5069416 (25.11%) aligned concordantly 0 times
    14068726 (69.69%) aligned concordantly exactly 1 time
    1049758 (5.20%) aligned concordantly >1 times
    ----
    5069416 pairs aligned concordantly 0 times; of these:
      887257 (17.50%) aligned discordantly 1 time
    ----
    4182159 pairs aligned 0 times concordantly or discordantly; of these:
      8364318 mates make up the pairs; of these:
        5857169 (70.03%) aligned 0 times
        2069665 (24.74%) aligned exactly 1 time
        437484 (5.23%) aligned >1 times
85.49% overall alignment rate
[bam_sort_core] merging from 16 files and 4 in-memory blocks...
