We recommend using the xeon partition (rather than general or himem) to run this application (hisat2)
19506047 reads; of these:
  19506047 (100.00%) were paired; of these:
    4489451 (23.02%) aligned concordantly 0 times
    13664358 (70.05%) aligned concordantly exactly 1 time
    1352238 (6.93%) aligned concordantly >1 times
    ----
    4489451 pairs aligned concordantly 0 times; of these:
      634777 (14.14%) aligned discordantly 1 time
    ----
    3854674 pairs aligned 0 times concordantly or discordantly; of these:
      7709348 mates make up the pairs; of these:
        5512260 (71.50%) aligned 0 times
        1785026 (23.15%) aligned exactly 1 time
        412062 (5.34%) aligned >1 times
85.87% overall alignment rate
[bam_sort_core] merging from 16 files and 4 in-memory blocks...
