We recommend using the xeon partition (rather than general or himem) to run this application (hisat2)
/usr/bin/env: python3: No such file or directory
20582596 reads; of these:
  20582596 (100.00%) were paired; of these:
    4586195 (22.28%) aligned concordantly 0 times
    14632531 (71.09%) aligned concordantly exactly 1 time
    1363870 (6.63%) aligned concordantly >1 times
    ----
    4586195 pairs aligned concordantly 0 times; of these:
      736948 (16.07%) aligned discordantly 1 time
    ----
    3849247 pairs aligned 0 times concordantly or discordantly; of these:
      7698494 mates make up the pairs; of these:
        5306629 (68.93%) aligned 0 times
        1905154 (24.75%) aligned exactly 1 time
        486711 (6.32%) aligned >1 times
87.11% overall alignment rate
[bam_sort_core] merging from 16 files and 4 in-memory blocks...
