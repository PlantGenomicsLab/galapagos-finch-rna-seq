We recommend using the xeon partition (rather than general or himem) to run this application (hisat2)
20958059 reads; of these:
  20958059 (100.00%) were paired; of these:
    5349591 (25.53%) aligned concordantly 0 times
    14360426 (68.52%) aligned concordantly exactly 1 time
    1248042 (5.95%) aligned concordantly >1 times
    ----
    5349591 pairs aligned concordantly 0 times; of these:
      919837 (17.19%) aligned discordantly 1 time
    ----
    4429754 pairs aligned 0 times concordantly or discordantly; of these:
      8859508 mates make up the pairs; of these:
        6237504 (70.40%) aligned 0 times
        2161202 (24.39%) aligned exactly 1 time
        460802 (5.20%) aligned >1 times
85.12% overall alignment rate
[bam_sort_core] merging from 16 files and 4 in-memory blocks...
