#!/bin/bash
#SBATCH --job-name=hisat2
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=grace.vaziri@uconn.edu
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=10G
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --array=[0-70]%30


echo "host name : " `hostname`
echo This is array task number $SLURM_ARRAY_TASK_ID
date


# load software----------------------------------------------
module load hisat2/2.2.1       
module load samtools/1.10

# set input/output files, directories------------------------

# input fastq file directory
INDIR=/home/FCAM/gvaziri/results/trimmomatic_outputs

# output directory
OUTDIR=../../results/hisat2results/alignments
mkdir -p $OUTDIR

# reference genome
GENOME=/isg/shared/databases/alignerIndex/animal/geospiza_fortis/current/HISAT2/geospiza_fortis

# fastq file bash array
FQS=($(find ${INDIR} -name "*L001_R1_001.paired.fastq.gz" | sort))

# fastq files
FQ1=${FQS[$SLURM_ARRAY_TASK_ID]}
echo $FQ1

# get corresponding sample ID
SAM=$(basename $FQ1 | sed 's/L00.*_R.*.paired.fastq.gz//')
echo $SAM

# specify fq files
fq1=$FQ1
fq2=$(echo $fq1 | sed 's/R1_001.paired.fastq.gz/R2_001.paired.fastq.gz/')
#fq1b=$(echo $fq1 | sed 's/L001_R1_001.paired.fastq.gz/L002_R1_001.paired.fastq.gz/')
#fq2b=$(echo $fq1 | sed 's/L001_R1_001.paired.fastq.gz/L002_R2_001.paired.fastq.gz/')

echo $fq1
echo $fq2
#echo $fq1b
#echo $fq2b

#outdir
outdir=$OUTDIR

# reference genome
genome=$GENOME

# output root, directory

outroot=$SAM
echo $outroot


#execute alignment and pipe to samtools
 
 hisat2 -x $GENOME -1 ${fq1} -2 ${fq2} | \
 samtools view -S -h -u - | \
 samtools sort -@ 4 -T $outdir/$outroot - \
 >$outdir/$outroot.bam

# index bam file
 samtools index $outdir/$outroot.bam
