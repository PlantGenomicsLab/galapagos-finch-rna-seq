#!/bin/bash
#SBATCH --job-name=trimming_rawreads
#SBATCH -n 1 
#SBATCH -N 1
#SBATCH -c 14
#SBATCH --mem=100G
#SBATCH --time 16:00:00
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=grace.vaziri@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

######################################################################
# Trimming of Reads using Trimmomatic 
#################################################################
## Changing directory with merged raw reads
cd ~/merged_raw_reads

module load Trimmomatic/0.39

for infile in *_R1_001.fastq.gz
 do
 base=$(basename ${infile} _R1_001.fastq.gz)
 java -jar $Trimmomatic PE -threads 14 -phred33 ${infile} ${base}_R2_001.fastq.gz \
 ${base}_R1_001.paired.fastq.gz ${base}_R1_001.unpaired.fastq.gz \
 ${base}_R2_001.paired.fastq.gz ${base}_R2_001.unpaired.fastq.gz \
 ILLUMINACLIP:/home/FCAM/gvaziri/scripts/TruSeq3-PE.fa:2:30:10 LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:45
done

#output directory
OUTDIR=~/results/trimmomatic_outputs
mkdir -p $OUTDIR

#mv outputs to new directory
mv *paired.fastq.gz $OUTDIR
