#!/bin/bash
#SBATCH --job-name=multiqc_trimmed
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=grace.vaziri@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
echo `hostname`
#################################################################
# MULTIQC of trimmed reads
#################################################################
module load MultiQC/1.1
cd /home/FCAM/gvaziri/results/merged_fastqc_outputs/trimmed/paired/

#output directory
OUTDIR=~/results/paired_multiqc_outputs/trimmed
mkdir -p $OUTDIR

multiqc --outdir $OUTDIR .
