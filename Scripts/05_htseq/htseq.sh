#!/bin/bash
#SBATCH --job-name=htseq9Feb
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=grace.vaziri@uconn.edu
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --array=[0-70]%20
echo "host name : " `hostname`
echo This is array task number $SLURM_ARRAY_TASK_ID
date
# load software----------------------------------------------
module load htseq/0.13.5
#cd into input directory
cd ~/results/hisat2results/alignments
# output directory
OUTDIR=~/results/htseq_counts
mkdir -p $OUTDIR

# bam file bash array
BAM=($(find -name "*_.bam"))
# bam files
BAM1=${BAM[$SLURM_ARRAY_TASK_ID]}
echo $BAM1

# get corresponding sample ID
SAM=$(basename $BAM1 | sed 's/_.bam//')
echo $SAM
#execute htseq command
htseq-count -s no -r pos -f bam --type exon $BAM1 GCF_000277835.1_GeoFor_1.0_genomic.gtf > $OUTDIR/$SAM.counts
