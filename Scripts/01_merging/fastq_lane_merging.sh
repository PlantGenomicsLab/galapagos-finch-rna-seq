#!/bin/bash
#SBATCH --job-name=merging_fastqs
#SBATCH -n 1 
#SBATCH -N 1
#SBATCH -c 14
#SBATCH --mem=10G
#SBATCH --time 2:00:00
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=grace.vaziri@uconn.edu

cd ~/raw_reads

for i in $(find ./ -type f -name "*.fastq.gz" | while read F; do basename $F | rev | cut -c 22- | rev; done | sort | uniq)

    do echo "Merging R1"

cat "$i"_L00*_R1_001.fastq.gz >> ../merged_raw_reads/"$i"_ME_L001_R1_001.fastq.gz

       echo "Merging R2"

cat "$i"_L00*_R2_001.fastq.gz >> ../merged_raw_reads/"$i"_ME_L001_R2_001.fastq.gz

done;
