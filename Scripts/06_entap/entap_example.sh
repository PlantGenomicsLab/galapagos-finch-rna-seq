#!/bin/bash
#SBATCH --job-name=entap
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=150G
#SBATCH --qos=general
#SBATCH -o entap_%j.out
#SBATCH -e entap_%j.err
#SBATCH --partition=general

module load anaconda/2.4.0
module load perl/5.30.1
module load diamond/0.9.25
module load interproscan/5.25-64.0
module load TransDecoder/5.3.0

/labs/Wegrzyn/EnTAP/EnTAP_v0.10.8/EnTAP/EnTAP --runP --ini /PATH_TO_INI_FILE  -i /PATH_TO_INPUT_TRANSCRIPTOME -d /PATH_TO_DIAMOND_DATABASE_1 -d /PATH_TO_DIAMOND_DATABASE_2 --threads 8 --out-dir /PATH_TO_OUTPUT_DIR -a /PATH_TO_SAM_FILE
